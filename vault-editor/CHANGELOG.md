# 0.0.12

- Change default `tmpPath` to `/tmp/vault_editor/` (was previously `./tmp`)
- Support Ansible config files and make them the default way of finding password files

# 0.0.14

- Fix broken diff command
- Also list remote branches when using diff command
